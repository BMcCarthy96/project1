import './GamesContainer.module.css'
import Games from '../Games'

function GamesContainer() {
    return (
        <>
        <Games />
        <p>This is the games container</p>
        </>
    )
}

export default GamesContainer


import './App.css'
import Header from './components/Header'
import Footer from './components/Footer'
import GamesContainer from './components/GamesContainer'

function App() {


  return (
    <>
    <Header />
     <GamesContainer />
    <Footer />
    </>
  )
}

export default App
